package main

import (
	"bufio"
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"os"
	"regexp"
	"strings"
	"time"
)

// Structure definition of the log file string
type logRecord struct {
	DateTime time.Time
	Index    string
	Level    string
	Script   string
	Divided  []string
	Message  []string
}

// Slice of parsed records
var logRecords []*logRecord

// Vars for input and output files flags
var fileLog string
var fileJson string

// Default file names
const (
	defaultFilelog  = "kamailio_0.log"
	defaultFilejson = "kamailio_0.json"
)

// Vars for filter flags
var startTime string
var stopTime string
var indexKey string
var levelKey string
var dividedKey string
var messageKey string

// Vars to use MongoDB
var client *mongo.Client
var coll *mongo.Collection

// Determine mongoDB collection name for log records
//var col = client.Database("test").Collection("kazoologs")

// Command line flags definition
func init() {
	flag.StringVar(&fileLog, "file-log", defaultFilelog, "an input log file name")
	flag.StringVar(&fileLog, "fl", defaultFilelog, "an input log file name (shorthand)")
	flag.StringVar(&fileJson, "file-json", defaultFilejson, "an output json file name")
	flag.StringVar(&fileJson, "fj", defaultFilejson, "an output json file name (shorthand)")
	flag.StringVar(&startTime, "start-time", "", "log start date/time in ISODate format (like 2006-01-02T15:04:05:00Z)")
	flag.StringVar(&stopTime, "stop-time", "", "log stop date/time in ISODate format (like 2006-01-02T15:04:05:00Z)")
	flag.StringVar(&indexKey, "i", " ", "index of log file records")
	flag.StringVar(&levelKey, "l", "", "log level of log file records")
	flag.StringVar(&dividedKey, "d", "", "string contained in Divided field to filter")
	flag.StringVar(&messageKey, "m", "", "string contained in Message fields to filter")
}

// Parsing log file and fill records collection
func fillCollection() {

	// Open log file
	file, err := os.Open(fileLog)
	if err != nil {
		log.Fatalf("Failed opening file: %s", err)
	}

	// Buffer input date as strings and call parser for each one
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		getParsedLogRecord(scanner.Text())
	}
	if err := scanner.Err(); err != nil {
		log.Fatalf("Error while reading file: %s", err)
	}
	if err := file.Close(); err != nil {
		log.Fatalf("Failed closing file: %s", err)
	}
}

// Log file strings parsing and logRecord objects return
func getParsedLogRecord(record string) logRecord {
	re, _ := regexp.Compile(`^(?P<datetime>[a-zA-Z]{3}\s{1,2}\d{1,2}\s\d{2}:\d{2}:\d{2})\s(kazoo kamailio?(\[(?P<index>[0-9]*)\]){0,1}):\s(?P<log_level>\w+):\s(?P<script>.*?):\s(?P<msg>.*)`)
	dateRegexp := regexp.MustCompile(`\s+`)
	rec := logRecord{}
	res := re.FindAllStringSubmatch(record, -1)
	for _, subMatches := range res {
		for key, regExpGroupName := range re.SubexpNames() {
			if regExpGroupName == "datetime" {
				rec.DateTime, _ = time.Parse("2006 Jan 2 15:04:05", time.Now().Format("2006 ")+dateRegexp.ReplaceAllString(strings.Trim(subMatches[key], " "), " "))
			}
			if regExpGroupName == "index" {
				rec.Index = subMatches[key]
			}
			if regExpGroupName == "log_level" {
				rec.Level = subMatches[key]
			}
			if regExpGroupName == "script" {
				rec.Script = subMatches[key]
			}
			if regExpGroupName == "msg" {
				message := strings.Split(subMatches[key], ": ")
				rec.Divided = strings.Split(message[0], "|")
				rec.Message = message[1:]
			}
		}
	}
	logRecords = append(logRecords, &rec)
	return rec
}

// Connect to MongoDB
func openDatabase() {

	// Open connection
	var err error
	client, err = mongo.Connect(context.TODO(), options.Client().ApplyURI("mongodb://localhost:27017"))
	if err != nil {
		log.Fatal(err)
	}

	// Check the connection
	err = client.Ping(context.Background(), nil)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Connected to MongoDB!")

	// Determine mongoDB collection name for log records
	coll = client.Database("test").Collection("kazoologs")
}

// Close the connection to MongoDB
func closeDatabase() {
	err := client.Disconnect(context.Background())
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Connection to MongoDB closed.")
}

// Save records collection to database
func saveCollectionToDB(records []*logRecord) {
	for _, rec := range records {
		// insertResult, err := coll.InsertOne(context.Background(), rec)
		_, err := coll.InsertOne(context.Background(), rec)
		if err != nil {
			log.Fatal(err)
		}
		// fmt.Println("Inserted a single record: ", insertResult.InsertedID)
	}
}

// Database cleaning
func cleanCollection() {
	filter := bson.M{}
	deleteResult, err := coll.DeleteMany(context.Background(), filter)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Deleted %v documents in the kazoologs collection\n", deleteResult.DeletedCount)
}

// Save file in JSON format
func saveJsonFile(str string) {
	if (fileLog != defaultFilelog) && (fileJson == defaultFilejson) {
		fileJson = fileLog[:strings.IndexByte(fileLog, '.')] + ".json"
	}
	file, err := os.Create(fileJson)
	if err != nil {
		log.Fatalf("Failed creating file: %s", err)
	}
	_, err = file.WriteString(str)
	if err != nil {
		log.Fatalf("Failed writing to file: %s", err)
	}
	if err := file.Close(); err != nil {
		log.Fatalf("Failed closing file: %s", err)
	}
}

// Database filter construction as bson.D type, bson.D{} matches all records in the collection
func filterCollection() bson.D {
	filterSlice := bson.D{}
	if indexKey != " " {
		filterSlice = append(filterSlice, bson.E{Key: "index", Value: indexKey})
	}
	if levelKey != "" {
		filterSlice = append(filterSlice, bson.E{Key: "level", Value: levelKey})
	}
	if startTime != "" {
		st, _ := time.Parse(time.RFC3339, startTime)
		filterSlice = append(filterSlice, bson.E{Key: "datetime", Value: bson.M{"$gte": st}})
	}
	if stopTime != "" {
		st, _ := time.Parse(time.RFC3339, stopTime)
		filterSlice = append(filterSlice, bson.E{Key: "datetime", Value: bson.M{"$lte": st}})
	}
	if dividedKey != "" {
		filterSlice = append(filterSlice, bson.E{Key: "divided", Value: bson.M{"$regex": dividedKey}})
	}
	if messageKey != "" {
		filterSlice = append(filterSlice, bson.E{Key: "message", Value: bson.M{"$regex": messageKey}})
	}

	return filterSlice
}

// Database filtered date receiving with bson filter
func getDBData(filter bson.D) []*logRecord {
	// filter := bson.D{}
	var results []*logRecord
	cur, err := coll.Find(context.Background(), filter)
	if err != nil {
		log.Fatal(err)
	}
	for cur.Next(context.Background()) {
		// create a value into which the single document can be decoded
		var elem logRecord
		err := cur.Decode(&elem)
		if err != nil {
			log.Fatal(err)
		}
		results = append(results, &elem)
	}
	if err := cur.Err(); err != nil {
		log.Fatal(err)
	}

	// Close the cursor once finished
	if err := cur.Close(context.Background()); err != nil {
		log.Fatal(err)
	}
	return results
}

// Main function
func main() {

	// Parse the command line flags
	flag.Parse()

	// Parse the log file
	fillCollection()

	// Open connection to MongoDB
	openDatabase()

	// Save collection to Database
	saveCollectionToDB(logRecords)

	// Construct database records filter
	filter := filterCollection()

	// Get filtered data from DB
	logRecords = getDBData(filter)

	// Convert data from structure format to json string
	jsonData, _ := json.Marshal(logRecords)

	// Output data to the stdout in json format
	fmt.Println(string(jsonData))

	// Save jsonData to file
	saveJsonFile(string(jsonData))

	// Clean Temporary Database
	cleanCollection()

	// Close connection to MongoDB
	closeDatabase()
}
