# GoLang KAZOO Log Parser

KAZOO service generates log files with separated string records, which has no defined format except some fields. An example log file is included to repository as *"kamailio.log"*.  
The parser can parse log files, make JSON output to stdout and file and filter records according to command lines flags. One can specify with flags file names for both input and output files, start and stop time of records and fields' content. Multiple flags operate filter as query with AND condition.   
Parser uses NoSQL MongoDB database with default local settings as temporary data storage.  Filter uses database queries to select data needed.   

Usage of ./main:
   
    -d string
          string contained in Divided field to filter
    -file-json string
          an output json file name (default "kamailio_0.json")
    -file-log string
          an input log file name (default "kamailio_0.log")
    -fj string
          an output json file name (shorthand) (default "kamailio_0.json")
    -fl string
          an input log file name (shorthand) (default "kamailio_0.log")
    -i string
          index of log file records (default " ")
    -l string
          log level of log file records
    -m string
          string contained in Message fields to filter
    -start-time string
          log start date/time in ISODate format (like 2006-01-02T15:04:05:00Z)
    -stop-time string
          log stop date/time in ISODate format (like 2006-01-02T15:04:05:00Z)

An example of the parser usage:

    $ ./main -start-time "2022-12-06T10:00:00Z" -stop-time "2022-12-06T10:05:00Z" -i "4014" -d "source"

